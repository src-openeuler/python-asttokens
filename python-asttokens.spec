Name:           python-asttokens
Version:        2.4.1
Release:        1
Summary:        Module to annotate Python abstract syntax trees with source code positions

License:        Apache-2.0
URL:            https://github.com/gristlabs/asttokens
Source0:        https://files.pythonhosted.org/packages/45/1d/f03bcb60c4a3212e15f99a56085d93093a497718adf828d050b9d675da81/asttokens-2.4.1.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-setuptools_scm
BuildRequires:	python3-pbr
BuildRequires:	python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-hatchling
BuildRequires:  python3-toml
BuildRequires:  python3-pytest
BuildRequires:  python3-astroid
BuildRequires:  python3-six
BuildRequires:  python3-typing-extensions

%global _description %{expand:
The asttokens module annotates Python abstract syntax trees (ASTs)
with the positions of tokens and text in the source code that
generated them. This makes it possible for tools that work with
logical AST nodes to find the particular text that resulted in those
nodes, for example for automated refactoring or highlighting.}

%description %_description


%package -n python3-asttokens
Summary:        %{summary}
Requires:       python3-six
%{?python_provide:%python_provide python3-asttokens}

%description -n python3-asttokens %_description


%prep
%autosetup -p1 -n asttokens-%{version}


%build
%pyproject_build


%install
%pyproject_install

%check
%pytest

%files -n python3-asttokens
%license LICENSE
%doc README.rst
%{python3_sitelib}/*


%changelog
* Fri Nov 17 2023 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 2.4.1-1
- Upgrade to version 2.4.1 and change the build method from py3 to pyproject

* Wed Mar 29 2023 li_zengyi <zengyi@isrc.iscas.ac.cn> - 2.2.1-1
- Init asttokens 2.2.1 package
